MySQL Partitioning	ZaBBIX(2.X.X)
1 MySQL Database Partitioning
	To dispel some of the myths regarding Zabbix and MySQL partitioning - It is in fact supported in Zabbix 2.0 and 2.2, 
	just not for all the tables that could previously be partitioned in 1.8. MySQL does not support partitioning 
	on tables that use foreign keys. The history and trend tables in Zabbix 2.0 and 2.2 
	do not use foreign keys,therefore partitioning is allowed on those tables.
2 Step-by-Step
	2.1 Connect to the Zabbix database
		mysql -h <database_ip/fqdn> -u <user> -p
		mysql>use zabbix;
	2.2 Index changes
		mysql> Alter table history_text drop primary key, add index (id), drop index history_text_2, add index history_text_2 (itemid, id);
		mysql> Alter table history_log drop primary key, add index (id), drop index history_log_2, add index history_log_2 (itemid, id);
	2.3 Stored Procedures

DELIMITER $$
CREATE PROCEDURE `partition_create`(SCHEMANAME varchar(64), TABLENAME varchar(64), PARTITIONNAME varchar(64), CLOCK int)
BEGIN
        /*
           SCHEMANAME = The DB schema in which to make changes
           TABLENAME = The table with partitions to potentially delete
           PARTITIONNAME = The name of the partition to create
        */
        /*
           Verify that the partition does not already exist
        */

        DECLARE RETROWS INT;
        SELECT COUNT(1) INTO RETROWS
        FROM information_schema.partitions
        WHERE table_schema = SCHEMANAME AND table_name = TABLENAME AND partition_description >= CLOCK;

        IF RETROWS = 0 THEN
                /*
                   1. Print a message indicating that a partition was created.
                   2. Create the SQL to create the partition.
                   3. Execute the SQL from #2.
                */
                SELECT CONCAT( "partition_create(", SCHEMANAME, ",", TABLENAME, ",", PARTITIONNAME, ",", CLOCK, ")" ) AS msg;
                SET @sql = CONCAT( 'ALTER TABLE ', SCHEMANAME, '.', TABLENAME, ' ADD PARTITION (PARTITION ', PARTITIONNAME, ' VALUES LESS THAN (', CLOCK, '));' );
                PREPARE STMT FROM @sql;
                EXECUTE STMT;
                DEALLOCATE PREPARE STMT;
        END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `partition_drop`(SCHEMANAME VARCHAR(64), TABLENAME VARCHAR(64), DELETE_BELOW_PARTITION_DATE BIGINT)
BEGIN
        /*
           SCHEMANAME = The DB schema in which to make changes
           TABLENAME = The table with partitions to potentially delete
           DELETE_BELOW_PARTITION_DATE = Delete any partitions with names that are dates older than this one (yyyy-mm-dd)
        */
        DECLARE done INT DEFAULT FALSE;
        DECLARE drop_part_name VARCHAR(16);

        /*
           Get a list of all the partitions that are older than the date
           in DELETE_BELOW_PARTITION_DATE.  All partitions are prefixed with
           a "p", so use SUBSTRING TO get rid of that character.
        */
        DECLARE myCursor CURSOR FOR
                SELECT partition_name
                FROM information_schema.partitions
                WHERE table_schema = SCHEMANAME AND table_name = TABLENAME AND CAST(SUBSTRING(partition_name FROM 2) AS UNSIGNED) < DELETE_BELOW_PARTITION_DATE;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

        /*
           Create the basics for when we need to drop the partition.  Also, create
           @drop_partitions to hold a comma-delimited list of all partitions that
           should be deleted.
        */
        SET @alter_header = CONCAT("ALTER TABLE ", SCHEMANAME, ".", TABLENAME, " DROP PARTITION ");
        SET @drop_partitions = "";

        /*
           Start looping through all the partitions that are too old.
        */
        OPEN myCursor;
        read_loop: LOOP
                FETCH myCursor INTO drop_part_name;
                IF done THEN
                        LEAVE read_loop;
                END IF;
                SET @drop_partitions = IF(@drop_partitions = "", drop_part_name, CONCAT(@drop_partitions, ",", drop_part_name));
        END LOOP;
        IF @drop_partitions != "" THEN
                /*
                   1. Build the SQL to drop all the necessary partitions.
                   2. Run the SQL to drop the partitions.
                   3. Print out the table partitions that were deleted.
                */
                SET @full_sql = CONCAT(@alter_header, @drop_partitions, ";");
                PREPARE STMT FROM @full_sql;
                EXECUTE STMT;
                DEALLOCATE PREPARE STMT;

                SELECT CONCAT(SCHEMANAME, ".", TABLENAME) AS `table`, @drop_partitions AS `partitions_deleted`;
        ELSE
                /*
                   No partitions are being deleted, so print out "N/A" (Not applicable) to indicate
                   that no changes were made.
                */
                SELECT CONCAT(SCHEMANAME, ".", TABLENAME) AS `table`, "N/A" AS `partitions_deleted`;
        END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `partition_maintenance`(SCHEMA_NAME VARCHAR(32), TABLE_NAME VARCHAR(32), KEEP_DATA_DAYS INT, HOURLY_INTERVAL INT, CREATE_NEXT_INTERVALS INT)
BEGIN
        DECLARE OLDER_THAN_PARTITION_DATE VARCHAR(16);
        DECLARE PARTITION_NAME VARCHAR(16);
        DECLARE OLD_PARTITION_NAME VARCHAR(16);
        DECLARE LESS_THAN_TIMESTAMP INT;
        DECLARE CUR_TIME INT;

        CALL partition_verify(SCHEMA_NAME, TABLE_NAME, HOURLY_INTERVAL);
        SET CUR_TIME = UNIX_TIMESTAMP(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'));

        SET @__interval = 1;
        create_loop: LOOP
                IF @__interval > CREATE_NEXT_INTERVALS THEN
                        LEAVE create_loop;
                END IF;

                SET LESS_THAN_TIMESTAMP = CUR_TIME + (HOURLY_INTERVAL * @__interval * 3600);
                SET PARTITION_NAME = FROM_UNIXTIME(CUR_TIME + HOURLY_INTERVAL * (@__interval - 1) * 3600, 'p%Y%m%d%H00');
                IF(PARTITION_NAME != OLD_PARTITION_NAME) THEN
			CALL partition_create(SCHEMA_NAME, TABLE_NAME, PARTITION_NAME, LESS_THAN_TIMESTAMP);
		END IF;
                SET @__interval=@__interval+1;
                SET OLD_PARTITION_NAME = PARTITION_NAME;
        END LOOP;

        SET OLDER_THAN_PARTITION_DATE=DATE_FORMAT(DATE_SUB(NOW(), INTERVAL KEEP_DATA_DAYS DAY), '%Y%m%d0000');
        CALL partition_drop(SCHEMA_NAME, TABLE_NAME, OLDER_THAN_PARTITION_DATE);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `partition_verify`(SCHEMANAME VARCHAR(64), TABLENAME VARCHAR(64), HOURLYINTERVAL INT(11))
BEGIN
        DECLARE PARTITION_NAME VARCHAR(16);
        DECLARE RETROWS INT(11);
        DECLARE FUTURE_TIMESTAMP TIMESTAMP;

        /*
         * Check if any partitions exist for the given SCHEMANAME.TABLENAME.
         */
        SELECT COUNT(1) INTO RETROWS
        FROM information_schema.partitions
        WHERE table_schema = SCHEMANAME AND table_name = TABLENAME AND partition_name IS NULL;

        /*
         * If partitions do not exist, go ahead and partition the table
         */
        IF RETROWS = 1 THEN
                /*
                 * Take the current date at 00:00:00 and add HOURLYINTERVAL to it.  This is the timestamp below which we will store values.
                 * We begin partitioning based on the beginning of a day.  This is because we don't want to generate a random partition
                 * that won't necessarily fall in line with the desired partition naming (ie: if the hour interval is 24 hours, we could
                 * end up creating a partition now named "p201403270600" when all other partitions will be like "p201403280000").
                 */
                SET FUTURE_TIMESTAMP = TIMESTAMPADD(HOUR, HOURLYINTERVAL, CONCAT(CURDATE(), " ", '00:00:00'));
                SET PARTITION_NAME = DATE_FORMAT(CURDATE(), 'p%Y%m%d%H00');

                -- Create the partitioning query
                SET @__PARTITION_SQL = CONCAT("ALTER TABLE ", SCHEMANAME, ".", TABLENAME, " PARTITION BY RANGE(`clock`)");
                SET @__PARTITION_SQL = CONCAT(@__PARTITION_SQL, "(PARTITION ", PARTITION_NAME, " VALUES LESS THAN (", UNIX_TIMESTAMP(FUTURE_TIMESTAMP), "));");

                -- Run the partitioning query
                PREPARE STMT FROM @__PARTITION_SQL;
                EXECUTE STMT;
                DEALLOCATE PREPARE STMT;
        END IF;
END$$
DELIMITER ;

	2.4 Using the stored procedures (Template:)
		
		mysql> CALL partition_maintenance('<zabbix_db_name>', '<table_name>', <days_to_keep_data>, <hourly_interval>, <num_future_intervals_to_create>)
		
		OK, on to actually running the stored procedures. The commands shown are using parameters that most people would use, but you may want something different. Change the parameters as you see fit.
		
		mysql> CALL partition_maintenance('zabbix', 'history', 28, 24, 14);
		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606080000,1465401600) |
		+-----------------------------------------------------------+
		1 row in set (0.08 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606090000,1465488000) |
		+-----------------------------------------------------------+
		1 row in set (0.15 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606100000,1465574400) |
		+-----------------------------------------------------------+
		1 row in set (0.19 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606110000,1465660800) |
		+-----------------------------------------------------------+
		1 row in set (0.24 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606120000,1465747200) |
		+-----------------------------------------------------------+
		1 row in set (0.28 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606130000,1465833600) |
		+-----------------------------------------------------------+
		1 row in set (0.32 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606140000,1465920000) |
		+-----------------------------------------------------------+
		1 row in set (0.37 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606150000,1466006400) |
		+-----------------------------------------------------------+
		1 row in set (0.41 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606160000,1466092800) |
		+-----------------------------------------------------------+
		1 row in set (0.46 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606170000,1466179200) |
		+-----------------------------------------------------------+
		1 row in set (0.50 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606180000,1466265600) |
		+-----------------------------------------------------------+
		1 row in set (0.56 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606190000,1466352000) |
		+-----------------------------------------------------------+
		1 row in set (0.61 sec)

		+-----------------------------------------------------------+
		| msg                                                       |
		+-----------------------------------------------------------+
		| partition_create(zabbix,history,p201606200000,1466438400) |
		+-----------------------------------------------------------+
		1 row in set (0.66 sec)

		+----------------+--------------------+
		| table          | partitions_deleted |
		+----------------+--------------------+
		| zabbix.history | N/A                |
		+----------------+--------------------+
		1 row in set (0.70 sec)

		Query OK, 0 rows affected (0.70 sec)

		mysql> 


	2.5 Improving procedure calls
	
DELIMITER $$
CREATE PROCEDURE `partition_maintenance_all`(SCHEMA_NAME VARCHAR(32))
BEGIN
                CALL partition_maintenance(SCHEMA_NAME, 'history', 28, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'history_log', 28, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'history_str', 28, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'history_text', 28, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'history_uint', 28, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'trends', 730, 24, 14);
                CALL partition_maintenance(SCHEMA_NAME, 'trends_uint', 730, 24, 14);
END$$
DELIMITER ;

			mysql> CALL partition_maintenance_all('zabbix');
			+----------------+--------------------+
			| table          | partitions_deleted |
			+----------------+--------------------+
			| zabbix.history | N/A                |
			+----------------+--------------------+
			1 row in set (0.01 sec)

			....
			....
			....

			+--------------------+--------------------+
			| table              | partitions_deleted |
			+--------------------+--------------------+
			| zabbix.trends_uint | N/A                |
			+--------------------+--------------------+
			1 row in set (22.85 sec)

			Query OK, 0 rows affected, 1 warning (22.85 sec)

			mysql>
			
	2.6 Housekeeper changes
		2.6.2 Zabbix 2.2.x

			All of the options are available in the Zabbix UI in the "Administration" -> "General" section. 
			Make sure you select "Housekeeping" in the drop-down in the upper right. 
			You should see a screen similar to the following:
			
			Administration-->General-->Housekeeping:
				
				Events and alerts:
					Enable internal housekeeping:enable
					Trigger data storage period (in days):180
					Internal data storage period (in days):180
					Network discovery data storage period (in days):1
					Auto-registration data storage period (in days):1
				IT services:
					Enable internal housekeeping:enable
					Data storage period (in days):90
				Audit:
					Enable internal housekeeping:enable
					Data storage period (in days):180
				User sessions:
					Enable internal housekeeping:enable
					Data storage period (in days):60
				History:
					Enable internal housekeeping:disable
					Override item history period:enable
					Data storage period (in days):28
				Trends:
					Enable internal housekeeping:disable
					Override item trend period:enable
					Data storage period (in days):730
					
		Don't forget to click Save!
	2.7 All done
		
		添加计划任务：每隔14天运行partition_maintenance_all存储过程
		全局配置之MySQL
		mysql>show variables like 'event_scheduler';		查询是否开启crond

		SET GLOBAL event_scheduler = 1;						临时开启

		echo "event_scheduler = 1" >>/etc/my.cnf			永久开启

		创建计划任务步骤：

		选择数据库并新建事件

		定义：
		BEGIN
		CALL partition_maintenance_all('zabbix');
		END

		定义者：
			root@%
			状态：enable

		计划:
			AT:One time task
			EVERY:Periodic planning task

		EVERY:
				14	DAY
			STARTS:enable
		save